//
//  AllListsVC.swift
//  Checklist App
//
//  Created by Ahmed adham on 8/15/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import UIKit

class AllListsVC: UITableViewController , ListDetailDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

 
    var lists : [CheckList]
    
    
    required init?(coder aDecoder: NSCoder) {
       
        lists = [CheckList]()
        
        super.init(coder: aDecoder)
        
        
        
        var list = CheckList(name: "Birthdays")
        lists.append(list)
        
        list = CheckList(name: "Groceries")
        lists.append(list)
        
        list = CheckList(name: "Cool Apps")
        lists.append(list)
        
        list = CheckList(name: "To Do")
        lists.append(list)
        
        for list in lists {
            let item = CheckListItem()
            item.text = "List item for \(list.name)"
            list.items.append(item)
        }
        
        loadChecklists()
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return lists.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = makeCell(for: tableView)
        let checklist = lists[indexPath.row]
        cell.textLabel?.text = checklist.name
        cell.accessoryType = .detailDisclosureButton
        return cell
    }
    

    func makeCell(for : UITableView) -> UITableViewCell {
        let cellIdentifier = "Cell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier){
            return cell
        }
        else {
            return UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let checklist = lists[indexPath.row]
        performSegue(withIdentifier: "showChecklist", sender: checklist)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showChecklist" {
            let controller = segue.destination as! ChecklistVC
            controller.checklist = sender as! CheckList
        }
        
        if segue.identifier == "addChecklist" {
            let navigator = segue.destination as! UINavigationController
            let controller = navigator.topViewController as! ListDetailVC
            controller.delegate = self
            controller.listToEdit = nil
            
        }
    }
    
   
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        lists.remove(at: indexPath.row)
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths , with: .automatic)
        saveChecklists()
    }
    
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        let navigationController = storyboard?.instantiateViewController(withIdentifier: "ListDetailNav") as! UINavigationController
        let controller = navigationController.topViewController as! ListDetailVC
        controller.delegate = self
        let checklist = lists[indexPath.row]
        controller.listToEdit = checklist
        present(navigationController, animated: true, completion: nil)
    
    }
    
    
    //Conforms Delegate
    
    func listDetailViewControllerDidCancel(_ controller: ListDetailVC) {
        dismiss(animated: true, completion: nil)
    }
    
    func listDetailViewController(_ controller: ListDetailVC, didFinishAdding checklist: CheckList) {
        
        let newRowIndex = lists.count
        lists.append(checklist)
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic)
        saveChecklists()
        dismiss(animated: true, completion: nil)
    }
    func listDetailViewController(_ controller: ListDetailVC, didFinishEditing checklist: CheckList) {
        if let index = lists.index(of: checklist) {
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) {
                cell.textLabel!.text = checklist.name
            }
        }
        saveChecklists()
        dismiss(animated: true, completion: nil)
    }
    
    
    //File
    
    func documentDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func dataFilePath() -> URL {
        return documentDirectory().appendingPathComponent("Checklists.plist")
    }
    
    func saveChecklists() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        
        archiver.encode(lists, forKey: "Checklists")
        archiver.finishEncoding()
        
        data.write(to: dataFilePath(), atomically: true)
    }
    
    func loadChecklists() {
        if let data = try? Data(contentsOf: dataFilePath()){
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            lists = unarchiver.decodeObject(forKey: "Checklists") as! [CheckList]
            unarchiver.finishDecoding()
        }
    }

    
}
