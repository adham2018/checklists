//
//  CheckListItem.swift
//  Checklist App
//
//  Created by Ahmed adham on 8/6/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import Foundation

class CheckListItem : NSObject , NSCoding{
    var text = "Walk the dog"
    var checked = false
    
    
    func toggleChecked() {
        checked = !checked
    }
    
    //ConformProtocol NSCoding with 2 methods
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(text, forKey: "Text")
        aCoder.encode(checked, forKey: "Checked")
    }
    
    required init?(coder aDecoder: NSCoder) {
        text = aDecoder.decodeObject(forKey: "Text") as! String
        checked = aDecoder.decodeBool(forKey: "Checked")
        super.init()
    }
    
    override init() {
        super.init()
    }
}
