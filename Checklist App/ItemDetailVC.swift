//
//  ItemDetailVC.swift
//  Checklist App
//
//  Created by Ahmed adham on 8/8/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import UIKit

protocol itemDetailVCdelegate : class {
    func itemDetailVCdidCancel(_: ItemDetailVC)
    func itemDetailVC(_:ItemDetailVC , didFinishedAdding item : CheckListItem)
    func itemDetailVC(_:ItemDetailVC , didFinishedEditing item : CheckListItem)
    
}

class ItemDetailVC: UITableViewController , UITextFieldDelegate {
 
  
    
    @IBOutlet weak var addItemTextField: UITextField!
    
    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    
    weak var delegate : itemDetailVCdelegate?
    
    var itemToEdit : CheckListItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = itemToEdit {
            title = "Edit Item"
            doneBarButton.isEnabled = true
            addItemTextField.text = item.text
        }
    }
    
    @IBAction func cancel(){
             delegate?.itemDetailVCdidCancel(self)
    }
    
    
    
    @IBAction func done(){
        
        if let item = itemToEdit {
            item.text = addItemTextField.text!
            delegate?.itemDetailVC(self, didFinishedEditing: item)
            print("Content of text edited : \(addItemTextField.text!)")
        }
        else {
        let item = CheckListItem()
        item.text = addItemTextField.text!
        item.checked = false
        delegate?.itemDetailVC(self, didFinishedAdding: item)
        print("Contents of the text field: \(addItemTextField.text!)")
        }
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addItemTextField.becomeFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let oldText = addItemTextField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string) as NSString
        doneBarButton.isEnabled = (newText.length > 0)
        return true
    }
}
