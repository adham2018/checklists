//
//  ListDetailVC.swift
//  Checklist App
//
//  Created by Ahmed adham on 8/16/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import UIKit

protocol ListDetailDelegate : class {
    
    func listDetailViewControllerDidCancel(_ controller: ListDetailVC)
    func listDetailViewController(_ controller: ListDetailVC , didFinishAdding checklist: CheckList)
    func listDetailViewController(_ controller: ListDetailVC , didFinishEditing checklist: CheckList)
}

class ListDetailVC: UITableViewController , UITextFieldDelegate{

    
    @IBOutlet weak var doneBarBtn: UIBarButtonItem!
    
    @IBOutlet weak var listDetailTextField: UITextField!
    
    weak var delegate : ListDetailDelegate?
    
    var listToEdit : CheckList?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let list = listToEdit {
            title = "Edit Checklist"
            listDetailTextField.text = list.name
            doneBarBtn.isEnabled = true
        }
    }


    @IBAction func done(_ sender: Any) {

        if let checklist = listToEdit {
             checklist.name = listDetailTextField.text!
             delegate?.listDetailViewController(self, didFinishEditing: checklist)
        }
        else {
            let checklist = CheckList(name: listDetailTextField.text!)
            delegate?.listDetailViewController(self, didFinishAdding: checklist)
        }
    }
 
    @IBAction func cancel(_ sender: Any) {
        delegate?.listDetailViewControllerDidCancel(self)
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        listDetailTextField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, willselectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = listDetailTextField.text! as NSString
        let newText = oldText.replacingCharacters(in: range, with: string ) as NSString
        
        doneBarBtn.isEnabled = (newText.length > 0)
        return true
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
