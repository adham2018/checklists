//
//  ViewController.swift
//  Checklist App
//
//  Created by Ahmed adham on 7/18/18.
//  Copyright © 2018 Ahmed adham. All rights reserved.
//

import UIKit

class ChecklistVC: UITableViewController , itemDetailVCdelegate {
   
    
   /* var row0text = "Walk the dog"
    var row1text = "Brush teeth"
    var row2text = "Learn iOS development"
    var row3text = "Soccer practice"
    var row4text = "Eat ice cream"
    
    var row0checked = false
    var row1checked = true
    var row2checked = false
    var row3checked = false
    var row4checked = true */
    
    var items : [CheckListItem] = []
    var checklist : CheckList!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = checklist.name
    }
    
    
  /*  required init?(coder aDecoder: NSCoder) {
        
        items = [CheckListItem]()
        super.init(coder: aDecoder)
        
        let row0item = CheckListItem()
        row0item.text = "Walk the dog"
        row0item.checked = false
        items.append(row0item)
        
        let row1item = CheckListItem()
        row1item.text = "Brush my teeth"
        row1item.checked = true
        items.append(row1item)
        
        let row2item = CheckListItem()
        row2item.text = "Learn iOS development"
        row2item.checked = true
        items.append(row2item)
        
        let row3item = CheckListItem()
        row3item.text = "Soccer practice"
        row3item.checked = false
        items.append(row3item)
        
        let row4item = CheckListItem()
        row4item.text = "Eat ice cream"
        row4item.checked = true
        items.append(row4item) 
        
        //loadChecklistItem()
     
       // print("Documents folder is \(documentsDirectory())")
       // print("Data file path is \(dataFilePath())")
    }
    */
    
   
    func configureCheckMark(for cell : UITableViewCell , with item : CheckListItem){
        let label = cell.viewWithTag(1001) as! UILabel
        
        if item.checked {
        label.text = "✔︎"
        }
        else {
            label.text = ""
        }
        
    }
    
    func configureText(for cell : UITableViewCell , with item : CheckListItem){
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = item.text
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklist.items.count
    }
    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "checkListItem", for: indexPath)
        
        let item = checklist.items[indexPath.row]
    
        configureCheckMark(for: cell, with: item)
        configureText(for: cell, with: item)
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
       
        if let cell = tableView.cellForRow(at: indexPath){
            let item = checklist.items[indexPath.row]
            item.toggleChecked()
            configureCheckMark(for: cell, with: item)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
       // saveChecklistItems()
    }
    
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        checklist.items.remove(at: indexPath.row)
        let indexPaths = [indexPath]
        tableView.deleteRows(at: indexPaths, with: .automatic)
       //saveChecklistItems()
        
    }
    
    //conformDelegates
    
    func itemDetailVCdidCancel(_: ItemDetailVC) {
        dismiss(animated: true, completion: nil)
    }
    
    func itemDetailVC(_: ItemDetailVC, didFinishedEditing item: CheckListItem) {
        if let index = checklist.items.index(of: item){
            let indexPath = IndexPath(row: index, section: 0)
            if let cell = tableView.cellForRow(at: indexPath){
                configureText(for: cell, with: item)
            }
            
        }
      //saveChecklistItems()
        dismiss(animated: true, completion: nil)
    }
    
    func itemDetailVC(_: ItemDetailVC, didFinishedAdding item: CheckListItem) {
        let newRowIndex = checklist.items.count
        item.checked = true
        checklist.items.append(item)
        let indexPath = IndexPath(row: newRowIndex, section: 0)
        let indexPaths = [indexPath]
        tableView.insertRows(at: indexPaths, with: .automatic)
        //saveChecklistItems()
        dismiss(animated: true, completion: nil)
    }
       
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "addItem" {
            let navController = segue.destination as! UINavigationController
            let controller = navController.topViewController as! ItemDetailVC
            controller.delegate = self
        }
        
        else if segue.identifier == "editItem" {
            let navController = segue.destination as! UINavigationController
            let controller = navController.topViewController as! ItemDetailVC
            controller.delegate = self
            
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell) {
                controller.itemToEdit = checklist.items[indexPath.row]
            }
        }
    }
    
    //File
    
   /*
   func documentsDirectory()-> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
   func dataFilePath() -> URL {
        
        return documentsDirectory().appendingPathComponent("Checklists.plist")
    }
 
    func saveChecklistItems(){
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(items, forKey: "CheckListItems")
        archiver.finishEncoding()
        data.write(to: dataFilePath(), atomically: true)
        
    }
    
    

    
    func loadChecklistItem(){
        let path = dataFilePath()
        
        if let data = try? Data(contentsOf : path) {
            
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            items = unarchiver.decodeObject(forKey: "CheckListItems") as! [CheckListItem]
            unarchiver.finishDecoding()
            
        }
    }
    */
   

}

